# allow-json

## Name

`allow-json` is a tool to generate JSON parsers in C for a given JSON spec.

## Description

The generated parser is in clean C, so easy to review it to confirm that it meets the requirement. No reference counting or any other complicated memory management in the parser. The JSON spec is given in a DSL, which is more readable the JSON itself.

## An example
Given a JSON spec as below
```
prefix("myjson_");
type addr_book struct {
    name string;
    age int;
    is_student("isStudent") bool;
    address struct {
        street string;
        city string;
        zip_code("zipCode") string;
    };
    phones("phoneNumbers") array(string);
};
```
Save it in a file named `my-great-spec` and run the command `/path/to/allow-json < my-great-spec > my-great-parser.h`
The generated parser can parse the JSON below and save the value to a C struct `myjson_addr_book`.
```
{
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "street": "123 Main St",
        "city": "Anytown",
        "zipCode": "12345"
    },
    "phoneNumbers": ["123-456-7890", "456-789-0123"]
}
```
## Project status

The code should come soon.